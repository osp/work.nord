(function () {

  class GridEditor {
    constructor (root, pageCount) {
      this.root = root;
      this.gridRowCount = parseInt(getComputedStyle(this.root).getPropertyValue('--rows'));
      this.gridColumnCount = parseInt(getComputedStyle(this.root).getPropertyValue('--columns'));
      this.pageCount = pageCount;

      this.draw();
    }


    templates = [
      function (page) {
        var textBlock = document.createElement('section');
        textBlock.classList.add('textblock');
        textBlock.dataset.flowFrom = 'text-flow-1';
        textBlock.style.gridRow = 'page-' + page + '-start / page-' + page + '-end';
        textBlock.style.gridColumn = 'span 2 / right';
    
        return [ textBlock ];
      }
    ]


    addElement (element) {
      this.root.appendChild(element);
    }


    generateGridRowsDeclaration () {
      var declaration = '';
  
      for (let p = 1; p <= this.pageCount; p++) {
        if (p == 1) {
          declaration = '[page-1-sheet-start page-1-padding-top start] var(--padding-top)\n';
        }
        
        for (let r = 1; r <= this.gridRowCount; r++) {
          let rowLineName = 'page-' + p + '-' + r;
          
          if (r == 1) {
            rowLineName += ' page-' + p + '-start'
          }
          
          declaration +=  '[' + rowLineName +  '] calc((var(--sheet-height) - var(--padding-top) - var(--padding-bottom)) / var(--rows))\n';
        }
        
        declaration += '[page-' + p + '-end page-' + p + '-padding-bottom] var(--padding-bottom)\n';
        
        if (p == this.pageCount) {
          // Last page. Add 'final' line
          declaration += '[page-' + p + '-sheet-end end]'
        } else {
          // Add line for end of current page and start of the next one
          declaration += '[page-' + p + '-sheet-end page-' + (p + 1) + '-sheet-start page-' + (p + 1) + '-padding-top] var(--padding-top)\n';
        }
      }
  
      return declaration;
    }


    setGridRowCount (rowCount) {
      this.gridRowCount = rowCount;
      this.root.style.setProperty('--rows', rowCount)
    }
  

    setGridColumnCount (columnCount) {
      this.gridColumnCount = columnCount;
      this.root.style.setProperty('--columns', columnCount);
    }


    setPageCount (pageCount) {
      this.pageCount = pageCount;
    }


    draw () {
      let rowsDeclaration = this.generateGridRowsDeclaration(),
          pageSeparators = this.root.querySelectorAll('.page-separator'),
          textBlocks = this.root.querySelectorAll('.textblock');

      for (let s = (pageSeparators.length - 1); s > -1 ; s--) {
        pageSeparators[s].remove();
      }

      for (let t = (textBlocks.length - 1); t > -1; t--) {
        textBlocks[t].remove();
      }

      this.root.style.gridTemplateRows = rowsDeclaration;

      for (let p = 1; p <= this.pageCount; p++) {
        
        const pageElements = this.templates[0](p);

        pageElements.forEach((e) => { this.addElement(e) });

        const separatorStart = document.createElement('div');
        separatorStart.classList.add('page-separator');
        separatorStart.style.gridRow = 'page-' + p + '-sheet-start' 
        this.addElement(separatorStart);

        const separatorEnd = document.createElement('div');
        separatorEnd.classList.add('page-separator');
        separatorEnd.style.gridRow = 'span 1 / page-' + p + '-sheet-end'
        this.addElement(separatorEnd);
      }
    }
  }

  window.GridEditor = GridEditor
})();