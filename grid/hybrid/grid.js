(function () {
  function initGrid (selector) {
    // set cols
    var setOrder = (element) => (value) => {
      element.style.order = parseInt(value);
    }

    var setCols = (element) => (value) => {
      element.style.gridColumnEnd = 'span ' + parseInt(value).toString();
    }

    // set rows
    var setRows = (element) => (value) => {
      element.style.gridRowEnd = 'span ' + parseInt(value).toString();
    }

    function makeControl(label, onChange, defaultValue) {
      var controlInput = document.createElement('input'),
          controlLabel = document.createElement('label');

      controlLabel.appendChild(document.createTextNode(label));
      controlInput.type = 'number';
      controlInput.value = parseInt(defaultValue);
      controlInput.addEventListener('change', function () { onChange(this.value) });

      controlLabel.appendChild(controlInput);

      return controlLabel;
    }

    // construct a control to set height and width
    // optionally positioning by setting grid row and col
    function addGridControls(element) {
      var rowControl = makeControl('Height', setRows(element), 1),
          colControl = makeControl('Width', setCols(element), 1),
          orderControl = makeControl('Order', setOrder(element), 0),
          container = document.createElement('section');

      container.classList.add('control--container');
      container.appendChild(rowControl);
      container.appendChild(colControl);
      container.appendChild(orderControl);
      element.appendChild(container);
    }

    document.querySelectorAll(selector).forEach((gridContainer) => {
      var gridItems = gridContainer.children;
      for (let i = 0; i < gridItems.length; i++) {
        addGridControls(gridItems[i]);
      }
    })
  }

  window.initGrid = initGrid;
})();