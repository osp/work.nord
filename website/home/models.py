from django.db import models

from wagtail.core.models import Page
from wagtail.core.fields import StreamField
from wagtail.admin.edit_handlers import StreamFieldPanel
from wagtail.core import blocks
from wagtail.snippets.blocks import SnippetChooserBlock

from projects.models import Quote, ProjectPage, Image

sizeBlockChoices = (
    (0, '1'),
    (1, '2'),
    (2, '3')
)

class HomePage(Page):
    body = StreamField([
      ('Quote', blocks.StructBlock([
        ('Quote', SnippetChooserBlock(Quote)),
        ('Size', blocks.ChoiceBlock(choices=sizeBlockChoices))
      ], template='home/partials/quote.html')),
      ('Project', blocks.StructBlock([
        ('Project', blocks.PageChooserBlock(ProjectPage)),
        ('Size', blocks.ChoiceBlock(choices=sizeBlockChoices))
      ], template='home/partials/project.html')),
      ('Image', blocks.StructBlock([
        ('Image', SnippetChooserBlock(Image)),
        ('Size', blocks.ChoiceBlock(choices=sizeBlockChoices))
      ], template='home/partials/image.html'))
    ])

    content_panels = Page.content_panels + [
        StreamFieldPanel('body', classname="full"),
    ]