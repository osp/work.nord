# Generated by Django 3.1.2 on 2021-03-19 09:20

from django.db import migrations, models
import django.db.models.deletion
import wagtail.core.fields
import wagtail.search.index


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailimages', '0022_uploadedimage'),
        ('projects', '0006_auto_20210318_1639'),
    ]

    operations = [
        migrations.CreateModel(
            name='Quote',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quote', wagtail.core.fields.RichTextField()),
                ('attribution', models.CharField(blank=True, max_length=250)),
            ],
            bases=(wagtail.search.index.Indexed, models.Model),
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('caption', wagtail.core.fields.RichTextField()),
                ('image', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='+', to='wagtailimages.image')),
            ],
            bases=(wagtail.search.index.Indexed, models.Model),
        ),
    ]
