# Generated by Django 3.1.2 on 2021-03-19 10:12

from django.db import migrations
import projects.models
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.images.blocks
import wagtail.snippets.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0008_auto_20210319_0922'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='projectpage',
            name='location',
        ),
        migrations.AlterField(
            model_name='project',
            name='body',
            field=wagtail.core.fields.StreamField([('heading', projects.models.HeadingBlock()), ('paragraph', wagtail.core.blocks.RichTextBlock()), ('quote', wagtail.core.blocks.BlockQuoteBlock()), ('quotesnippet', wagtail.snippets.blocks.SnippetChooserBlock('Quote')), ('datablock', wagtail.core.blocks.StreamBlock([('datarow', wagtail.core.blocks.StructBlock([('name', wagtail.core.blocks.ChoiceBlock(choices=[(0, 'Status'), (1, 'Procedure'), (2, 'Area'), (3, 'Budget'), (4, 'Team: Architecture'), (5, 'Team: Public space'), (6, 'Team: Landscape'), (7, 'Team: Structure Ir'), (8, 'Team: HVAC Ir'), (9, 'Team: Energy performance '), (10, 'Team: Acoustic '), (11, 'Team: Graphic Design'), (12, 'Team: Artist collaborator'), (13, 'Team: BIM manager'), (14, 'Team: Participation'), (15, 'Team: Design'), (16, 'Sub-contractor: Architecture'), (17, 'Sub-contractor: Public space'), (18, 'Sub-contractor: Landscape'), (19, 'Sub-contractor: Structure Ir'), (20, 'Sub-contractor: HVAC Ir'), (21, 'Sub-contractor: Energy performance '), (22, 'Sub-contractor: Acoustic '), (23, 'Sub-contractor: Graphic Design'), (24, 'Sub-contractor: Artist collaborator'), (25, 'Sub-contractor: BIM manager'), (26, 'Sub-contractor: Participation'), (27, 'Sub-contractor: Design'), (28, 'Built area'), (29, 'Renovated area'), (30, 'Surroundings area'), (31, 'Initial budget'), (32, 'Adjudication budget'), (33, 'Final budget'), (34, 'Study start'), (35, 'Expeted delivery date'), (36, 'Delivery date')], classname='field-col col6', form_classname='field-col col6')), ('value', wagtail.core.blocks.CharBlock(classname='field-col col6', form_classname='field-col col6'))])), ('dataheader', wagtail.core.blocks.CharBlock())], icon='table', template='projects/blocks/datablock.html')), ('plan', wagtail.images.blocks.ImageChooserBlock()), ('picture', wagtail.images.blocks.ImageChooserBlock()), ('gallery', wagtail.core.blocks.StreamBlock([('plan', wagtail.images.blocks.ImageChooserBlock()), ('picture', wagtail.images.blocks.ImageChooserBlock())]))]),
        ),
        migrations.AlterField(
            model_name='projectpage',
            name='body',
            field=wagtail.core.fields.StreamField([('Project', wagtail.snippets.blocks.SnippetChooserBlock('project'))]),
        ),
    ]
