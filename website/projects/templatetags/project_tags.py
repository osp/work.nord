from projects.models import get_project_datablock_choices, get_project_gallery_layout_names
from django import template

register = template.Library()

@register.filter
def get_datablock_datarow_display (value):
  choices = get_project_datablock_choices()
  
  return dict(choices)[int(value)]


@register.filter
def get_gallery_layout_name (value):
  names = get_project_gallery_layout_names()
  
  return dict(names)[int(value)]
